provide-module repl-buffer %{

declare-option -hidden str repl_buffer_source %sh{dirname "$kak_source"}
declare-option -hidden str repl_buffer_workdir
declare-option \
    -docstring "If true, text sent to a repl's input is echoed to the output." \
    bool repl_buffer_send_echo true
declare-option \
    -docstring "List of active REPL buffers, oldest to newest." \
    str-list repl_buffer_list

define-command repl-buffer-new \
    -params .. \
    -shell-completion \
    -docstring "
        repl-buffer-new [<switches>] [--] <cmd>: run shell command in a buffer
        <cmd>                 A shell command, like 'ls -al'

        Switches:
            -name <name>      Set the name of the buffer capturing the output
    " \
%{
    evaluate-commands %sh{
        fail() { printf "%s\n" "fail -- %{$*}"; exit 1; }

        bufname=""
        while [ $# -gt 0 ]; do
            case "$1" in
                -name)
                    shift
                    if [ $# -eq 0 ]; then fail "name switch needs a value"; fi
                    bufname="$1"
                    ;;
                --)
                    shift || fail "shell command required"
                    break
                    ;;
                -*)
                    fail "Unrecognised switch $1"
                    ;;
                *)
                    break
                    ;;
            esac
            shift
        done
        if [ "$#" -eq 0 ]; then
            fail "shell command required"
        fi
        if [ -z "$bufname" ]; then
            bufname="*$1*"
        fi

        workdir=$(mktemp -d "${TMPDIR:-/tmp}"/kak-repl-buffer.XXXXXXXX)
        mkfifo "$workdir/input"
        mkfifo "$workdir/output"
        (
            "$kak_opt_repl_buffer_source/repl-buffer-input" "$workdir/input" |
                "$@" >"$workdir/output" 2>&1 &
        ) </dev/null >/dev/null 2>&1

        printf %s\\n "
            edit! -fifo $workdir/output -scroll $bufname
            set-option buffer repl_buffer_workdir $workdir
            set-option -add global repl_buffer_list $bufname
            hook -always -once buffer BufClose .* %{ repl-buffer-close }
            hook -always -once buffer BufCloseFifo .* %{ repl-buffer-close }
        "
    }
}

define-command repl-buffer-close \
    -hidden \
    -params 0 \
%{
    evaluate-commands %sh{
        if [ -n "$kak_opt_repl_buffer_workdir" ]; then
            # We could use rm -rf, but for safety let's be explicit.
            rm "$kak_opt_repl_buffer_workdir"/input
            rm "$kak_opt_repl_buffer_workdir"/output
            rmdir "$kak_opt_repl_buffer_workdir"
        fi
        echo "unset buffer repl_buffer_workdir"
        echo "set-option -remove global repl_buffer_list $kak_bufname"
    }
}

define-command repl-buffer-send-text-raw \
    -params 2 \
    -docstring "
        repl-buffer-send-text-raw <buffer> <text>: Send <text> to REPL <buffer>
        <buffer> must be a buffer previously created by repl-buffer-new
    " \
%{
    evaluate-commands %sh{
        kakquote() { printf "%s\n" "$*"|sed -e "s/'/''/g;1s/^/'/;\$s/\$/' /"; }
        buffer=$(kakquote "$1")
        text=$(kakquote "$2")

        if [ x"$kak_opt_repl_buffer_send_echo" = xtrue ]; then
            # Echo the sent text to the buffer output.
            printf 'eval -buffer %s %%{
                echo -to-file "%%opt{repl_buffer_workdir}/output" %s
            }\n' "$buffer" "$text"
        fi

        # Actually send the input to the repl
        printf 'eval -buffer %s %%{
            echo -to-file "%%opt{repl_buffer_workdir}/input" %s
        }\n' "$buffer" "$text"
    }
}

define-command repl-buffer-send-text \
    -params ..1 \
    -docstring "
        repl-buffer-send-text [<text>]: Send text to the current REPL's input.
        If <text> is not supplied, sends the current selection.
        If the current buffer is not a REPL, use the most recent REPL.
        Also echoes text if the repl_buffer_send_echo option is true.
    " \
%{
    evaluate-commands %sh{
        kakquote() { printf "%s\n" "$*"|sed -e "s/'/''/g;1s/^/'/;\$s/\$/' /"; }

        # The text to send is the first parameter (if any)
        # or otherwise $kak_selection
        if [ $# -eq 0 ]; then
            text="$kak_selection"
        else
            text="$1"
        fi

        # If this is not an active repl buffer...
        if [ -z "$kak_opt_repl_buffer_workdir" ]; then
            # ...let's look for the most recently-launched repl
            eval set -- "$kak_quoted_opt_repl_buffer_list"
            if !shift $(( $# - 1 )); then
                printf "fail %s\n" "No repl buffers to send to"
                exit 1
            fi
            buffer="$1"
        else
            # This is an active repl buffer.
            buffer="$kak_bufname"
        fi

        # Now we have a buffer and text, send the text to the REPL buffer
        printf 'repl-buffer-send-text-raw %s %s\n' \
            "$(kakquote "$buffer")" "$(kakquote "$text")"
    }
}

define-command repl-buffer-prompt \
    -params 0 \
    -docstring "
        repl-buffer-prompt: Prompt for text and send it to the current REPL.
    " \
%{
    evaluate-commands %sh{
        if [ -z "$kak_opt_repl_buffer_workdir" ]; then
            printf "fail %s\n" "Not a repl buffer."
        fi
    }

    evaluate-commands -save-regs p %{
        # We assume the last line of the buffer is a prompt,
        # so we yank it into "p
        execute-keys -draft gjxH"py

        prompt %reg{p} %{
            # Send the text the user entered
            repl-buffer-send-text-raw %val{bufname} %val{text}

            # We need to send a newline, but we can't *type* a newline here,
            # we can't generate one with the shell (because shell escapes strip
            # newlines) so we'll have to get creative: we will yank the newline
            # at the end of the buffer and paste it into the command-line.
            evaluate-commands -save-regs n %{
                execute-keys -draft ge"ny
                repl-buffer-send-text-raw %val{bufname} %reg{n}
            }

            # When the REPL responds, give it a little time to respond,
            # then assume it's printed another prompt and do the dance again.
            hook -once -always buffer BufReadFifo .* %{
                hook -once -always buffer NormalIdle .* %{
                    repl-buffer-prompt
                }
            }
        }
    }
}

} # end module
