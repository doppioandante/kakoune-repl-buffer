A REPL inside a Kakoune buffer
==============================

A REPL is a "read, evaluate, print" loop;
a tool that takes commands or calculations from the user,
processes them,
then displays the results.
Common REPL tools include
command-line shells,
calculators,
database query tools,
and programming languages.

Kakoune includes the `:x11-repl` and `:tmux-repl` commands
to launch a REPL tool in a separate terminal,
but they depend on having X11 or tmux available.
Also,
because the REPL is launched in a separate terminal,
it's more difficult to get its output back into Kakoune.

Kakoune also includes the `:edit -fifo` command,
which lets you easily pipe the output of a command
into a new Kakoune buffer.
Unfortunately,
that only works with command *output*, not input,
and it requires special steps - you can't just give it a command to run.

This plugin provides the best of both worlds:
`:repl-buffer-new` takes a shell command
and automatically plumbs it into a Kakoune buffer,
but it also sets up an input pipe,
so `:repl-buffer-send-text` can communicate with it.

For more direct interactions,
you can also use `:repl-buffer-prompt`
which repeatedly prompts you for input
(using Kakoune's `:prompt` command)
and sends each response to the REPL,
like a regular terminal session.

TODO
----

  - Figure out how to make a mapping that calls `:repl-buffer-send-text`
    with the contents of the pending register,
    so we can map paste commands to it.
  - Things for the troubleshooting/tips section:
      - For a more Vim-like feel,
        alias `:!` to `:repl-buffer-new`
      - Many REPL tools hide their prompt and other output
        when run inside a pipe instead of a terminal.
        Many of those tools support `-i` or `--interactive`
        to force interactive behaviour anyway.
      - On some systems,
        tools run inside a pipe default to fully-buffered output,
        so they will not output anything until the tool exits,
        or until a few kilobytes of output has piled up.
        The `setbuf` tool (if it exists on your system)
        can disable that default bufffering.
